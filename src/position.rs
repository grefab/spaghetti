use path::Direction;

#[derive(Copy, Eq)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

impl Position {
    pub fn moved_in_direction(&self, d: &Direction) -> Position {
        match *d {
            Direction::N => Position {
                x: self.x,
                y: self.y - 1,
            },
            Direction::S => Position {
                x: self.x,
                y: self.y + 1,
            },
            Direction::E => Position {
                x: self.x + 1,
                y: self.y,
            },
            Direction::W => Position {
                x: self.x - 1,
                y: self.y,
            },
        }
    }

    pub fn translated(&self, t: Position) -> Position {
        Position {
            x: self.x + t.x,
            y: self.y + t.y,
        }
    }
}

impl Clone for Position {
    fn clone(&self) -> Position {
        *self
    }
}

impl PartialEq for Position {
    fn eq(&self, other: &Position) -> bool {
        self.x == other.x && self.y == other.y
    }
}
