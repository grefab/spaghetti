extern crate rand;

use path::rand::Rng;

use position::Position;

#[derive(Clone, Eq, PartialEq)]
pub enum Direction {
    N,
    S,
    E,
    W,
}

const OPTIONS: [Direction; 4] = [Direction::N, Direction::S, Direction::E, Direction::W];

fn rnd_dir() -> Direction {
    rand::thread_rng().choose(&OPTIONS).unwrap().clone()
}

#[derive(Clone, Eq, PartialEq)]
pub struct Path {
    pub start: Position,
    pub way: Vec<Direction>,
}

impl Path {
    pub fn new(start: Position) -> Path {
        Path {
            start,
            way: Vec::new(),
        }
    }

    pub fn random(start: Position, len: u32) -> Path {
        let mut path = Path::new(start);
        for _i in 0..len {
            path.add(rnd_dir());
        }

        path
    }

    pub fn add(&mut self, d: Direction) {
        self.way.push(d);
    }

    pub fn positions(&self) -> Vec<Position> {
        let mut result: Vec<Position> = Vec::new();
        result.push(self.start);
        self.way.iter().fold(self.start, |pos, dir| {
            let next_pos = pos.moved_in_direction(dir);
            result.push(next_pos);
            next_pos
        });

        result
    }

    pub fn mutated(&self) -> Path {
        let mut new_way = self.way.clone();

        let rnd: i32 = rand::thread_rng().gen_range(0, 100);
        let max_idx = new_way.len();

        if rnd < 20 || new_way.len() < 2 {
            // Add something
            new_way.insert(rand::thread_rng().gen_range(0, max_idx + 1), rnd_dir());
        } else if rnd < 80 {
            // Remove something
            new_way.remove(rand::thread_rng().gen_range(0, max_idx));
        } else {
            // Swap random elements
            new_way.swap(
                rand::thread_rng().gen_range(0, max_idx),
                rand::thread_rng().gen_range(0, max_idx),
            );
        }

        Path {
            start: self.start,
            way: new_way,
        }
    }
}
