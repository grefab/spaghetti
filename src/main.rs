mod engine;
mod gui;
mod path;
mod position;
mod world;

use std::io::Write;

fn main() {
    println!("Finding path...");

    let world = world::World::new(10, 10);
    let engine = engine::Engine::new();
    let mut gui = gui::Gui::new();

    {
        let mut population = engine.create_population();

        gui.clear();
        gui.draw_population(&population, &world, &engine);
        gui.update();

        for generation in 0.. {
            let new_population = engine.evolve(&population, &world);

            if generation % 100 == 0 {
                gui.clear();
                gui.draw_population(&new_population, &world, &engine);
                gui.update();
                print!("gen: {}: ", generation);

                new_population
                    .iter()
                    .map(|p| engine.score(p, &world))
                    .for_each(|score| {
                        print!("{}, ", score);
                    });
                println!();
                std::io::stdout().flush().unwrap();
            }

            population = new_population;

            if gui.key_pressed() {
                break;
            }
        }
    }
}
