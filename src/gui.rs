extern crate sdl2;

use engine::Engine;
use gui::sdl2::event::Event;
use gui::sdl2::keyboard::Keycode;
use gui::sdl2::pixels::Color;
use gui::sdl2::render::WindowCanvas;
use gui::sdl2::Sdl;
use path::Path;
use position::Position;
use std::time::Duration;
use world::World;

pub struct Gui {
    sdl_context: Sdl,
    canvas: WindowCanvas,
}

impl Gui {
    pub fn new() -> Gui {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();

        let window = video_subsystem
            .window("rust-sdl2 demo: Video", 710, 710)
            .position_centered()
            .opengl()
            .build()
            .unwrap();

        Gui {
            sdl_context,
            canvas: window.into_canvas().build().unwrap(),
        }
    }

    pub fn draw_population(&mut self, population: &Vec<Path>, world: &World, engine: &Engine) {
        let mut rated_population: Vec<(i32, Path)> = population
            .iter()
            .map(|path| (engine.score(path, world), path.clone()))
            .collect();
        rated_population.sort_by(|a, b| b.0.cmp(&a.0));

        let mut i = 0;
        for path in rated_population.iter().map(|e| &e.1) {
            let offset = Position {
                x: 10 + (i % 10) * 70,
                y: 10 + (i / 10) * 70,
            };
            let scale = 6;
            self.draw(offset, scale, path, world);

            i += 1;

            if i > 100 {
                return;
            }
        }
    }

    pub fn draw(&mut self, offset: Position, scale: i32, path: &Path, world: &World) {
        self.draw_world(world, offset, scale);
        self.draw_path(
            &path,
            offset.translated(Position {
                x: scale / 2,
                y: scale / 2,
            }),
            scale,
        )
    }

    fn draw_path(&mut self, path: &Path, offset: Position, scale: i32) -> () {
        let mut pos = path.start.clone();
        for dir in &path.way {
            let next_pos = pos.moved_in_direction(dir);
            self.draw_line(
                (offset.x + (pos.x * scale), offset.y + (pos.y * scale)),
                (
                    offset.x + (next_pos.x * scale),
                    offset.y + (next_pos.y * scale),
                ),
            );
            pos = next_pos;
        }
    }

    fn draw_world(&mut self, world: &World, offset: Position, scale: i32) {
        // Top
        self.draw_line(
            (offset.x + 0, offset.y + 0),
            (offset.x + (world.w as i32 * scale), offset.y + 0),
        );
        // Right
        self.draw_line(
            (offset.x + (world.w as i32 * scale), offset.y + 0),
            (
                offset.x + (world.w as i32 * scale),
                offset.y + (world.h as i32 * scale),
            ),
        );
        //Bottom
        self.draw_line(
            (
                offset.x + (world.w as i32 * scale),
                offset.y + (world.h as i32 * scale),
            ),
            (offset.x + 0, offset.y + (world.h as i32 * scale)),
        );
        // Left
        self.draw_line(
            (offset.x + 0, offset.y + (world.h as i32 * scale)),
            (offset.x + 0, offset.y + 0),
        );
    }

    pub fn clear(&mut self) {
        self.canvas.set_draw_color(Color::RGB(0, 0, 0));
        self.canvas.clear();
    }

    pub fn draw_line(&mut self, p1: (i32, i32), p2: (i32, i32)) {
        self.canvas.set_draw_color(Color::RGB(255, 255, 255));
        let _ = self.canvas.draw_line(p1, p2);
    }

    pub fn update(&mut self) {
        self.canvas.present();
    }

    pub fn key_pressed(&self) -> bool {
        let mut event_pump = self.sdl_context.event_pump().unwrap();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => return true,
                _ => {}
            }
        }

        return false;
    }

    pub fn wait_key(&self) {
        let mut event_pump = self.sdl_context.event_pump().unwrap();

        'running: loop {
            for event in event_pump.poll_iter() {
                match event {
                    Event::Quit { .. }
                    | Event::KeyDown {
                        keycode: Some(Keycode::Escape),
                        ..
                    } => {
                        break 'running;
                    }
                    _ => {}
                }
            }
            ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
        }
    }
}
