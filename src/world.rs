use path::Path;

#[allow(dead_code)]
pub struct World {
    pub w: u32,
    pub h: u32,
}

impl World {
    pub fn new(w: u32, h: u32) -> World {
        World { w, h }
    }

    pub fn is_valid(&self, path: &Path) -> bool {
        let positions = path.positions();

        // A position is considered valid if
        // - No position lies outside our world
        // - No position is visited twice.
        for pos in &positions {
            if pos.x < 0 || pos.y < 0 || pos.x >= self.w as i32 || pos.y >= self.h as i32 {
                return false;
            }
        }

        for pos in &positions {
            let count = positions.iter().filter(|p| *p == pos).count();
            if count > 1 {
                return false;
            }
        }

        true
    }
}
