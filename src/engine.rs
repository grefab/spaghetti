extern crate rand;

use engine::rand::Rng;
use path::Path;
use position::Position;
use std::cmp;
use world::World;

pub struct Engine {}

impl Engine {
    pub fn new() -> Engine {
        Engine {}
    }

    pub fn create_population(&self) -> Vec<Path> {
        let mut population: Vec<Path> = Vec::new();
        for _ in 0..100 {
            population.push(Path::random(Position { x: 0, y: 0 }, 5));
        }

        population
    }

    pub fn score(&self, path: &Path, world: &World) -> i32 {
        // Length is good for score.
        let mut score = path.way.len() as i32 * 2;

        // Corners are good for score.
        for i in 0..(path.way.len() - 1) {
            if path.way[i] != path.way[i + 1] {
                score += 1;
            }
        }

        // Violations are bad for score.
        if !world.is_valid(path) {
            score -= 1000;
        }
        score
    }

    pub fn evolve(&self, population: &Vec<Path>, world: &World) -> Vec<Path> {
        // - Have a population of 100
        // - Find uniques
        let unique_population = {
            let mut result: Vec<Path> = Vec::new();
            for path in population {
                if !result.contains(path) {
                    result.push(path.clone());
                }
            }

            result
        };

        // - Rate population
        let mut rated_population: Vec<(i32, Path)> = unique_population
            .iter()
            .map(|path| (self.score(path, world), path.clone()))
            .collect();
        rated_population.sort_by(|a, b| b.0.cmp(&a.0));

        // - Take over at most the best 20
        let mut new_population: Vec<Path> = Vec::new();
        for i in 0..(cmp::min(10, rated_population.len())) {
            new_population.push(rated_population[i].1.clone());
        }

        // - Mutate best 30 ten times
        for i in 0..(cmp::min(30, rated_population.len())) {
            let path = &rated_population[i].1;
            for _ in 0..10 {
                let mut new_path = path.mutated();
                for _ in 0..1 {
                    new_path = new_path.mutated();
                }
                new_population.push(new_path);
            }
        }

        // Throw in some random ones
        for _ in 0..10 {
            new_population.push(Path::random(
                Position { x: 0, y: 0 },
                rand::thread_rng().gen_range(5, 15),
            ));
        }

        new_population
    }
}
